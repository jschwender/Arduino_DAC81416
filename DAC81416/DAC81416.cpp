/*
 Texas Instruments DAC 81416

 
 Circuit:
 DAC 81416 attached to pins 6, 7, 10 - 13:
 DRDY: pin 6
 CSB: pin 7
 MOSI: pin 11
 MISO: pin 12
 SCK: pin 13

 ©2020 J. Schwender  */

#include <SPI.h>
#include "DAC81416.h"
SPISettings bus1 = SPISettings(4000000,MSBFIRST,SPI_MODE1);  // definiert bus1 als ein Datensatz für die Transmissionsparamter

void DAC81416::InitDAC() {
  // start the SPI library:
  SPI.begin();
#ifdef DEBUG_INIT_DAC
  Serial.print("InitDAC: Connct SPI: SS");
  Serial.print(SS, DEC);
  Serial.print(" MOSI: ");
  Serial.print(MOSI, DEC);
  Serial.print(" MISO: ");
  Serial.print(MISO, DEC);
  Serial.print(" SCK: ");
  Serial.println(SCK, DEC);
#edif
  bitSet(SPCR, 4);    //arduino is Master
  SPI.setBitOrder(MSBFIRST);      //bit transmission order
  SPI.setDataMode(SPI_MODE1);     // Setup at RE and Saple at FE
  SPI.setClockDivider(SPI_CLOCK_DIV4); //data rate = fosc/16 = 1 Mbit

  bitClear(SPCR, 7);              //local SPI interrupt is disable
  pinMode(chipSelectPin, OUTPUT);
  writeRegister(RegSPICONFIG, SPICONFIG_DEFAULT & ~(DEV_PWDWN));  // 0x0A84 power up device. achtung clock-polarität -> bit 2!  also SPICONFIG_DEFAULT & ~(DEV-PWDWN)
  SetAllDACsRange(Out0to10V); // 
//  SetAllDACsRange(0x1111); // 
  writeRegister(RegSYNCONFIG, 0x0000);  // set up outputs to synchronous mode (no LDAC needed)
  writeRegister(RegDACPWDWN, 0x0000); // power up all outputs
  writeRegister(RegGENCONFIG, GENCONFIG_DEFAULT & ~(REF_PWDWN)); // 0x3F00 activate internal reference
#ifdef DEBUG_INIT_DAC
  Serial.println(" end, ok");
#endif
}

//   Special case: all channels same range setting.
void DAC81416::SetAllDACsRange(uint16_t RCode) {
  uint16_t x = (RCode && 0x000f);
  x = x + (4<<x) + (8<<x) + (12<<x);
#ifdef DEBUG_SETALLDACSRANGE
    Serial.print("Set Range Registers: "); Serial.print(RCode,HEX); Serial.print(" --x4--> "); Serial.println(x, HEX);
#endif
//  uint16_t x = RCode;
  writeRegister(RegDACRANGE0, x);  // all channels set to given range
  writeRegister(RegDACRANGE1, x);
  writeRegister(RegDACRANGE2, x);
  writeRegister(RegDACRANGE3, x);
}
//  Range Code conform with names in manual RCodea is highest RCoded is lowest nibble
void DAC81416::SetDACsRange0(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded ) {
  writeRegister(RegDACRANGE0, (RCoded + (4<<RCodec) + (8<<RCodeb) + (12<<RCodea)));  // 4 channels set to individual range
}
void DAC81416::SetDACsRange1(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded ) {
  writeRegister(RegDACRANGE1, (RCoded + (4<<RCodec) + (8<<RCodeb) + (12<<RCodea)));  // 4 channels set to individual range
}
void DAC81416::SetDACsRange2(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded ) {
  writeRegister(RegDACRANGE2, (RCoded + (4<<RCodec) + (8<<RCodeb) + (12<<RCodea)));  // 4 channels set to individual range
}
void DAC81416::SetDACsRange3(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded ) {
  writeRegister(RegDACRANGE3, (RCoded + (4<<RCodec) + (8<<RCodeb) + (12<<RCodea)));  // 4 channels set to individual range
}

//Sends a int value to a DAC register

//Sendet eine Spannung in mV als int an eines der DAC register
void DAC81416::AusgangXmillivolt(byte Register, unsigned int mVolt) {
  long x = (mVolt * 65535) / 10000;  // 10000 mV --> 65535
  writeRegister(Register, x);
#ifdef DEBUG_AUSGANGXMILLIVOLT
    Serial.print("AusgangXmV: "+String(x));
#endif
}

void DAC81416::writeRegister(byte ZielRegister, unsigned int RegisterWert) {
#ifdef DEBUG_WRITEREGISTER
    Serial.print("writeRegister");
#endif
  digitalWrite(SS, LOW);       // ChipSelect (SS) aul LOW
  SPI.beginTransaction(bus1);  // bus1: es gibt ohnehin nur einen bus, im moment....
  SPI.transfer(ZielRegister);        //Send register location
  SPI.transfer16(RegisterWert);         //Send int value
  digitalWrite(SS, HIGH);  // take the chip select high to de-select:
  SPI.endTransaction();
#ifdef DEBUG_WRITEREGISTER
    Serial.println("end, ok");
#endif
}
