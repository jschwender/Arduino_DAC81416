/*
 Texas Instruments DAC 81416

 
 Circuit:
 DAC 81416 attached to pins 6, 7, 10 - 13:
 DRDY: pin 6
 CSB: pin 7
 MOSI: pin 11
 MISO: pin 12
 SCK: pin 13

 ©2020 J. Schwender  */

// the DAC communicates using SPI, so include the library:
#ifndef DAC81416_h
#define DAC81416_h
#include <SPI.h>

#define DEBUG_INIT_DAC
#define DEBUG_WRITEREGISTER
#define DEBUG_AUSGANGXMILLIVOILT
#define DEBUG_SETALLDACSRANGE

// pins used for the connection with the sensor
#define  RegNOP = 0x0         // def. 0x0000
#define  RegDEVICEID    0x01
#define  RegSTATUS      0x02  // def. 0x0000, Alarm & Busy bits
#define  RegSPICONFIG   0x03  // def. 0x0AA4, 
#define  RegGENCONFIG   0x04  // def. 0x7F00, clear bit 14 to activate internal ref.
#define  RegBRDCONFIG   0x05  // de. 0xFFFF
#define  RegSYNCONFIG   0x06  // def. 0x0000 -> asynchronous update mode
#define  RegTOGGCONFIG0 0x07  // def. 0x0000
#define  RegTOGGCONFIG1 0x08  // def. 0x0000
#define  RegDACPWDWN    0x09  // DAC Power-Down Register, def 0xFF -> all ch. power down, 0x00 -> all ch. normal operation
#define  RegDACRANGE0   0x0A  // DAC[15:12] Range Register, def. 0->[0…5V] 0x11 -> [0…10V]
#define  RegDACRANGE1   0x0B  // DAC[11:8]  Range Register, def. 0->[0…5V] 0x11 -> [0…10V]
#define  RegDACRANGE2   0x0C  // DAC[7:4]   Range Register, def. 0->[0…5V] 0x11 -> [0…10V]
#define  RegDACRANGE3   0x0D  // DAC[3:0]   Range Register, def. 0->[0…5V] 0x11 -> [0…10V]
#define  RegTRIGGER     0x0E  // Trigger Register
#define  RegBRDCAST     0x0F  // Broadcast Data Register
#define  RegDAC0  0x10  // CH0 data register
#define  RegDAC1  0x11  // CH1 data register
#define  RegDAC2  0x12  // …
#define  RegDAC3  0x13
#define  RegDAC4  0x14
#define  RegDAC5  0x15
#define  RegDAC6  0x16
#define  RegDAC7  0x17
#define  RegDAC8  0x18
#define  RegDAC9  0x19
#define  RegDAC10 0x1A
#define  RegDAC11 0x1B
#define  RegDAC12 0x1C
#define  RegDAC13 0x1D
#define  RegDAC14 0x1E
#define  RegDAC15 0x1F
#define  RegOFFSET0 0x20  // DAC[14-15;12-13] Differential Offset Register
#define  RegOFFSET1 0x21  // DAC[10-11;8-9] Differential Offset Register
#define  RegOFFSET2 0x22  // DAC[6-7;4-5] Differential Offset Register
#define  RegOFFSET3 0x23  // DAC[2-3;0-1] Differential Offset Register


#define Bit0  0x0001  // B0000000000000001
#define Bit1  0x0002  // B0000000000000010
#define Bit2  0x0004  // B0000000000000100
#define Bit3  0x0008  // B0000000000001000
#define Bit4  0x0010  // B0000000000010000
#define Bit5  0x0020  // B0000000000100000
#define Bit6  0x0040  // B0000000001000000
#define Bit7  0x0080  // B0000000010000000
#define Bit8  0x0100  // B0000000100000000
#define Bit9  0x0200  // B0000001000000000
#define Bit10 0x0400  // B0000010000000000
#define Bit11 0x0800  // B0000100000000000
#define Bit12 0x1000  // B0001000000000000
#define Bit13 0x2000  // B0010000000000000
#define Bit14 0x4000  // B0100000000000000
#define Bit15 0x8000  // B1000000000000000

// SPICONFIG Register (Offset = 03h) [reset = 0AA4h]
#define SPICONFIG_DEFAULT	0x0AA4
#define TEMPALM_EN	Bit11   // When set to 1 a thermal alarm triggers the ALMOUT pin.
#define DACBUSY_EN	Bit10   // When set to 1 the ALMOUT pin is set between DAC output updates. Contrary to other alarm events, this alarm resets automatically.
#define CRCALM_EN	Bit9    // When set to 1 a CRC error triggers the ALMOUT pin
#define SOFTTOGGLE_EN	Bit6
#define DEV_PWDWN	Bit5    // DEV_PWDWN = 1 sets the device in power-down mode
#define CRC_EN		Bit4    // When set to 1 frame error checking is enabled.
#define STR_EN	 	Bit3    // When set to 1 streaming mode operation is enabled.
#define SDO_EN	 	Bit2
#define FSDO   	Bit1
// GENCONFIG Register (Offset = 04h) [reset = 7F00h]
#define GENCONFIG_DEFAULT	0x7F00
#define REF_PWDWN      	Bit14  // REF_PWDWN = 1 powers down the internal reference
#define DAC_14_15_DIFF_EN	Bit7
#define DAC_12_13_DIFF_EN	Bit6   //When set to 1 the corresponding DAC pair is set to operate in differential mode. The DAC data registers must be rewritten after enabling or disabling differential operation.
#define DAC_10_11_DIFF_EN	Bit5
#define DAC_8_9_DIFF_EN	Bit4
#define DAC_6_7_DIFF_EN	Bit3
#define DAC_4_5_DIFF_EN	Bit2
#define DAC_2_3_DIFF_EN	Bit1
#define DAC_0_1_DIFF_EN	Bit0

//  Register (Offset = 09h) [reset = FFFFh]
#define DACPWDWN_DEFAULT	0xFFFF
#define DAC15_PWDWN	Bit15
#define DAC14_PWDWN	Bit14
#define DAC13_PWDWN	Bit13
#define DAC12_PWDWN	Bit12
#define DAC11_PWDWN	Bit11
#define DAC10_PWDWN	Bit10
#define DAC9_PWDWN	Bit9
#define DAC8_PWDWN	Bit8
#define DAC7_PWDWN	Bit7
#define DAC6_PWDWN	Bit6
#define DAC5_PWDWN	Bit5
#define DAC4_PWDWN	Bit4
#define DAC3_PWDWN	Bit3
#define DAC2_PWDWN	Bit2
#define DAC1_PWDWN	Bit1
#define DAC0_PWDWN	Bit0
// DACRANGEn Register (Offset = 0Ah - 0Dh) [reset = 0000h]
#define Out0to5V	B0000 // = 0 to 5 V
#define Out0to10V	B0001 // = 0 to 10 V
#define Out0to20V	B0010 // = 0 to 20 V
#define Out0to40V	B0100 // = 0 to 40 V
#define Outm5to5V	B1001 // = -5 V to +5 V
#define Outm10to10V	B1010 // = -10 V to +10 V
#define Outm20to20V	B1100 // = -20 V to +20 V
#define Outm2p5to2p5V	B1110 // = -2.5 V to +2.5 V
// TRIGGER Register (Offset = 0Eh) [reset = 0000h]
#define ALM_RESET	Bit8  // Set this bit to 1 to clear an alarm event. Not applicable for a DAC-BUSY alarm event.
//  not complete…
// BRDCAST Register (Offset = 0Fh) [reset = 0000h]
// OFFSETn Register (Offset = 20h - 23h) [reset = 0000h]

enum SensorTyp { Pos, Amp };

class DAC81416 {
public:
	int error;
	int chipSelectPin = 7;
	void InitDAC(void);
	void AusgangXmillivolt(byte Register, unsigned int mVolt);
	void writeRegister(byte ZielRegister, unsigned int Registerwert);
private:
        void SetAllDACsRange(uint16_t Code);
        void SetDACsRange0(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded );
        void SetDACsRange1(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded );
        void SetDACsRange2(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded );
        void SetDACsRange3(uint16_t RCodea, uint16_t RCodeb, uint16_t RCodec, uint16_t RCoded );
};

#endif